# frozen_string_literal: true

module Rodauth
  module OAuth
    VERSION = "0.6.0"
  end
end
